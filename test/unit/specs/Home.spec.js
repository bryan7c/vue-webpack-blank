import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    site: {
      title: 'Site com Vuex'
    }
  },
  mutations: {
    setTitle (state) {
      state.site.title = 'title'
    }
  }
})

import Home from '@/views/Home'

Home.store = store

describe('Home.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Home)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.home h1').textContent)
      .to.equal('Welcome to Your Vue.js App')
  })
})
