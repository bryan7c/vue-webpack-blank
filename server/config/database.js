let Datastore = require('nedb')
let dbName = 'data.db'
let db

if (!db) {
  db = new Datastore({
    filename: dbName,
    autoload: true
  })
}

module.exports = db
