var db = require('../../config/database')
var api = {}

api.adiciona = function (req, res) {
  db.insert(req.body, function (err, newDoc) {
    if (err) return console.log(err)
    console.log('Adicionado com sucesso: ' + newDoc._id)
    res.json(newDoc._id)
  })
}

module.exports = api
