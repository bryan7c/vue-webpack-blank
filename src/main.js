// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import vueValidator from 'vue-validator'
import App from './App'
import router from './router'
import store from './vuex/store'
import BootstrapVue from 'bootstrap-vue'
import Toasted from 'vue-toasted'
import ToastedMixin from './toasted'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'font-awesome/css/font-awesome.min.css'

/* ======= Resource ======= */
import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.config.productionTip = false
Vue.http.options.root = process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : 'http://' + window.location.host + process.env.PROD_URL
// TODO implementar variavel global para o loading
Vue.http.interceptors.push((request, next) => {
  // console.log('inicio')
  next((response) => {
    // console.log('fim')
  })
})
/* ======= Resource ======= */

/* ======= Filtros customizados ======= */
import * as customFilters from './filters'
Object.keys(customFilters).map(key => {
  Vue.filter(key, customFilters[key])
})
/* ======= Filtros customizados ======= */

Vue.use(BootstrapVue)
Vue.use(vueValidator)
Vue.use(Toasted, {router})
Vue.mixin(ToastedMixin)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  store
})
