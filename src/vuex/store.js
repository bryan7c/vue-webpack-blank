import Vue from 'vue'
import Vuex from 'Vuex'
import AircraftModel from '@/models/AircraftModel'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    aircraftModel: new AircraftModel()
  },
  mutations: {
    SET_ARCRAFT_MODEL (state, aircraftModel) {
      state.aircraftModel = new AircraftModel(aircraftModel)
    }
  },
  getters: {
    getAircraftModel: state => state.aircraftModel
  },
  actions: {
    setAircraftModel ({ commit }, aircraftModel) {
      commit('SET_ARCRAFT_MODEL', aircraftModel)
    }
  }
})
