import Service from '@/services/Service'
import File from '@/models/File'

export default class FileService extends Service {

  constructor (resource) {
    super(resource, 'api/filehistories/search/last-received{?fileType,aircraftModelId}')
  }

  postFile (aircraftModelId, fileType, file) {
    this.setUrl('api/filehistories')
    let data = new FormData()
    data.append('file', file)
    data.append('fileType', fileType)
    data.append('aircraftModelId', aircraftModelId)
    return new Promise((resolve, reject) => {
      this._service.save(data).then(response => {
        resolve(new File(response.body))
      }, err => {
        reject(err)
      })
    })
  }

  getFilesByAircraft (fileType, aircraftModelId) {
    this.setUrl('api/filehistories/search/last-received{?fileType,aircraftModelId}')
    return new Promise((resolve, reject) => {
      this._service.get({ fileType, aircraftModelId }).then(response => {
        resolve(new File(response.body))
      }, err => {
        reject(err)
      })
    })
  }
}
