export default class Service {
  constructor (resource, urlTemplate) {
    this._resource = resource
    this.setUrl(urlTemplate)
  }

  setUrl (url) {
    this._service = this._resource(url)
  }

  save (fup) {
    if (!fup.id) {
      return this._service
      .save(fup)
      .then(
        res => res,
        err => {
          if (err.status === 400) {
            return err.body
          } else {
            throw new Error(`${err.body.message}`)
          }
        }
      )
    } else {
      return this._service
      .update({}, fup)
      .then(
        res => res,
        err => {
          if (err.status === 400) {
            return err.body
          } else {
            throw new Error(`${err.body.message}`)
          }
        }
      )
    }
  }

  delete (id) {
    return this._service
    .delete({ id: id })
    .then(null,
      err => {
        console.error(err)
        throw new Error(`Could not delete ${this._model} with id: ${id} \nServer message: ${err.body.message}`)
      }
    )
  }

  getAll () {
    return this._service
    .query()
    .then(response => response.json(),
      err => {
        console.error(err)
        throw new Error(`Could not list all ${this._model}s \nServer message: ${err.body.message}`)
      }
    )
  }

  getById (id) {
    return this._service
    .query({ id: id })
    .then(response => response.json(),
      err => {
        console.error(err)
        throw new Error(`Could not get ${this._model} with id: ${id} \nServer message: ${err.body.message}`)
      }
    )
  }

  getByFilter (obj) {
    return this._service
    .query(obj)
    .then(
      res => res.json(),
      err => {
        console.error(err)
        throw new Error(`Could not get ${this._model} with query: ${obj} \nServer message: ${err.body.message}`)
      }
    )
  }
}
