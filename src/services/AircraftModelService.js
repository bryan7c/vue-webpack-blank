import Service from '@/services/Service'
import AircraftModel from '@/models/AircraftModel'

export default class AircraftModelService extends Service {

  constructor (resource) {
    super(resource, 'api/aircraftmodels{?page,size,sort}')
  }

  getAll () {
    return new Promise((resolve, reject) => {
      this._service.get().then(response => {
        let aircrafList = []
        response.body._embedded.aircraftmodels.forEach(aircraft => {
          aircrafList.push(new AircraftModel(aircraft))
        })
        resolve(aircrafList)
      }, err => {
        reject(err)
      })
    })
  }
}
