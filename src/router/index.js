import Vue from 'vue'
import Router from 'vue-router'

// lAZY LOADING
const ComponentView = resolve => require(['@/views/Component-view'], resolve)
const Home = resolve => require(['@/views/Home'], resolve)
const Webkit = resolve => require(['@/views/webkit/Webkit'], resolve)

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/component',
      name: 'ComponentView',
      component: ComponentView
    },
    {
      path: '*',
      name: 'Default',
      component: Home
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/webkit',
      name: 'Webkit',
      component: Webkit
    }
  ],
  mode: 'history',
  linkActiveClass: 'active',
  scrollBehavior: function (to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    }
    if (savedPosition) {
      return savedPosition
    } else {
      return {x: 0, y: 0}
    }
  }
})
