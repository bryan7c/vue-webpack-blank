export default class AircraftModel {
  constructor (obj = {}) {
    this.id = obj.id || ''
    this.code = obj.code || ''
    this.description = obj.description || ''
    this._links = obj._links || null
  }
}
