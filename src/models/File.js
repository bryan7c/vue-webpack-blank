class Links {
  constructor (obj = {}) {
    this.aircraftModel = obj.aircraftModel || {href: ''}
    this.file = obj.file || {href: ''}
    this.filehistory = obj.filehistory || {href: ''}
    this.self = obj.self || {href: ''}
  }
}
export default class File {
  constructor (obj = {}) {
    this.filename = obj.filename || ''
    this.md5 = obj.md5 || ''
    this.status = obj.status || ''
    this.dateTimeReceived = obj.dateTimeReceived || ''
    this.dateTimeProcessed = obj.dateTimeProcessed || ''
    this.fileType = obj.fileType || ''
    this.result = obj.result || []
    this.links = obj._links || new Links()
    this.file = null
  }
}
