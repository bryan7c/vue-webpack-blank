var toasterDuration = 5000
var toasterActionsDefault = {
  class: 'fa-times',
  text: '',
  onClick: (e, toastObject) => {
    toastObject.goAway(0)
  }
}

export default {
  methods: {
    toasterError (msg, duration = toasterDuration) {
      this.$toasted.show(msg, {
        duration,
        theme: 'alert-danger',
        action: [
          toasterActionsDefault
        ]
      })
    },
    toasterAlert (msg, duration = toasterDuration) {
      this.$toasted.show(msg, {
        duration,
        theme: 'alert-warning',
        action: [
          toasterActionsDefault
        ]
      })
    },
    toasterSuccess (msg, duration = toasterDuration) {
      this.$toasted.show(msg, {
        duration,
        theme: 'alert-success',
        action: [
          // {
          //   text: 'Home',
          //   router navigation
          //   push: {
          //     name: 'Home',
          //     dontClose: true
          //   }
          // },
          toasterActionsDefault
        ]
      })
    }
  }
}
