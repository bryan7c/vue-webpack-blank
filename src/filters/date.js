import moment from 'moment'
export default function (value, format = 'MMM DD, YYYY') {
  if (value) {
    return moment(value).format(format)
  }
}
